#include <set>
#include <iterator>
#include <iostream>
#include <exception>
#include <memory>
#include <vector>
#include <iterator>

class InvalidArg : public std::exception {
    public:
    const char *what() const throw() {
        return "Inmvalid Armgument of fumnction pleamse next time umse it more caremfully";
    }
};

template<typename A, typename V>
class FunctionMaxima {
private:
    template<typename T>
    class prot_iterator;
    class impl;
    std::unique_ptr<impl> pimpl;
    
    class association {
        private:
        friend class prot_iterator<association>;
        friend class InsertGuard;
        std::shared_ptr<A> _argument;
        std::shared_ptr<V> _value;

        // ten bool pod wskaźnikiem informje iteratory o ich poprawności
        // każdy iterator który nie jest już poprawny rzuci wyjątek przy
        // następnym użyciu
        std::shared_ptr<bool> validation;

        protected:
            association(A _arg, V _val){
                _argument.reset(&_arg);
                _value.reset(&_val);
                validation.reset(new bool);
                *validation = true;
            }

            // bzz ?
            association(const association &ass) {
                _argument = ass._argument;
                _value = ass._value;
                validation = ass.validation;
            }

        public:
            A const& arg() const {return *_argument;}
            V const& value() const {return *_value;}
            
            ~association() noexcept {
                *validation = false;
            }

            void change_val(V val) {
                _value.reset(&val);
            }
            
    };
    
    struct fun_comp {
        bool operator()(association &a, association &b){
            return a.arg() < b.arg();
        }
    };

    struct mx_comp {
        bool operator()(association &a, association &b){
            if (a.value() == b.value())
                return a.arg() < b.arg();
            return a.value() > b.value();
        }
    };
    using fun_set = std::set<association>;
    using mx_set = std::set<association>;

    fun_set function_set;
    mx_set maximas_set;

    /**
     * Obudowanie iteratora std::set w dodatkową ochronę przed niepożądanymi
     * operacjami jak end()++, begin()--, *end(), oraz ochrona przed używaniem
     * iteratora, który został unieważniony (w skutek set.clear() lub set.erease())
     * 
     * @throw InvalidatedIterator gdy iterator został unieważniony
     * @throw InvalidIteratorDereference gdy wykonujemy dereferencję iteratora 
     *        wskazującego za koniec setu
     * @throw InvalidIteratorMove gdy wykonujemy inkrementację (dekrementację)
     *        iteratora za końcem (na początku) setu
     * 
     * Spełnia wymogi std::bidirectional_iterator_tag
     * 
     * Dodatkowe info:
     *  zakładamy że T posiada "std::shared_ptr<bool> validation"
     */
    template<typename T>
    class prot_iterator : public std::iterator<std::bidirectional_iterator_tag, T>{
        using set_t = std::set<T>;
        
        std::shared_ptr<bool> validation;
        typename std::set<T>::iterator itt;
        bool past_the_end;
        const set_t *iterating;

        public:
        class InvalidatedIterator : public std::exception {
            public:
            const char *what() const throw() override {
                return "This iterator has been ivalidated due to cointainer management.";
            }
        };
        class InvalidIteratorDereference : public std::exception {
            public:
            const char *what() const throw() override {
                return "This iterator is pointing past the end of the container and cannot be dereferenced.";
            }
        };
        class InvalidIteratorMove : public std::exception {
            public:
            const char *what() const throw() override {
                return "Incrementation (decrementation) happened for past-the-end (begin) iterator what results in undefined behaviour.";
            }
        };
        private:
        void check_validation(bool dereferencing = false) const {
            // same as "ivalidated iterator" no matter the operation
            if (!past_the_end && *validation == false)
                throw InvalidatedIterator();
        }

        public:
        prot_iterator(typename std::set<T>::iterator &&itt, const set_t *iterating) noexcept{
            this->itt = itt;
            this->iterating = iterating;
            this->validation = (*itt).validation;
            this->past_the_end = (itt == iterating->end());
        }
        
        /** Zapisane stylem STL'a (zmieniamy?)**/
        public:
        const T &
        operator*() const {
            if (past_the_end)
                throw InvalidIteratorDereference();
            
            check_validation(true);
            return *itt;
        }
        const T *
        operator->() const {
            if (past_the_end)
                throw InvalidIteratorDereference();
            check_validation(true);
            return itt.operator->();
        }

        prot_iterator &
        operator++() {
            check_validation();
            // set przy usuwaniu usuwa swoje elementy czyli
            // ten kawałek kodu ruszy <=> set istnieje
            if (past_the_end)
                throw InvalidIteratorMove();

            ++itt;
            if (itt == iterating->end())
                past_the_end = true;
            
            if (!past_the_end)
                validation = (*itt).validation;

            return *this;
        }

        prot_iterator
        operator++(int) {
            iterator tmp = *this;
            ++(*this); // validation checking
            return tmp;
        }

        prot_iterator &
        operator--() {
            check_validation();

            if (itt == iterating->begin())
                throw InvalidIteratorMove();
            
            else if (past_the_end)
                past_the_end = false;

            --itt;

            if (!past_the_end)
                validation = (*itt).validation;

            return *this;
        }   

        prot_iterator
        operator--(int) {
            iterator tmp = *this;
            --(*this);
            return tmp;
        }

        bool
        operator==(const prot_iterator &x) const {
            check_validation();
            x.check_validation();
            return itt == x.itt;
        }

        bool
        operator!=(const prot_iterator &x) const {
            check_validation();
            x.check_validation();
            return itt != x.itt;
        }
    };

    public:
    using point_type = association;
    typedef typename std::set<point_type>::size_type  size_type;
    using mx_iterator = prot_iterator<point_type>;
    using iterator = prot_iterator<point_type>;

    // przy insercie tworzony
    // probuje insertowac i łapie wyjatki
    // jesli nie bylo wyjatkow - element inserted - rollback=true (jest co rollbackowac)
    // jesli wszystkie inserty przejdą to dropuje rollback i gdy sie destructuje nie cofa zmian
    class InsertGuard {
        private:
        using fun_set = FunctionMaxima<A, V>::fun_set;
        using mx_set = FunctionMaxima<A, V>::mx_set; 
        using point_type = FunctionMaxima<A, V>::point_type;
        using fun_it = typename fun_set::iterator;
        using mx_it = typename mx_set::iterator;
        
        fun_set *fun_set_ptr;
        mx_set *mx_set_ptr;
        fun_it *fun_its;
        mx_it *mx_its;
        mx_it *new_mx_its;
        bool *rollback;
        bool *is_max;

        bool is_maximum(typename fun_set::iterator itt){
            V *left, *right, *self;
            self = (*itt)._value.get();
            if (itt == fun_set_ptr->begin())
                left = self;
            else {
                itt--;
                left = (*itt)._value.get();
                itt++;
            }
            itt++;
            if (itt == fun_set_ptr->end())
                right = self;
            else {
                right = (*itt)._value.get();
            }
            itt--;
            return (*left <= *self && *self >= *right);   
        }

        void find_all(point_type &new_point) {
            fun_set function_set = *fun_set_ptr;
            mx_set maximas_set = *mx_set_ptr;
            fun_its[1] = function_set.find(new_point); // znajduje argument taki jak new_pointa

            if (fun_its[1] == function_set.end()) {
                // nie ma takiego argumentu jeszcze
                fun_its[2] = function_set.upper_bound(new_point);
                fun_its[0] = fun_its[2].prev();
            } else {
                // jest juz taki argument
                fun_its[2] = function_set.next();

                if (fun_its[1] == function_set.begin())
                    fun_its[0] = function_set.end();
                else
                    fun_its[0] = fun_its[1].prev();

                fun_its[2] = fun_its[1].next();
            }
            for (int i = 0; i < 3; i++) {
                if (fun_its[i] != function_set.end())
                    mx_its[i] = maximas_set.find(*(fun_its[i]));
            }
        }

        public:
        // jak zainicjalizowac tablice iteratorow?
        InsertGuard() : mx_set_ptr(nullptr), fun_set_ptr(nullptr) {
            fun_its = new fun_it[3]; // ??? 
            mx_its = new mx_it[3];
            new_mx_its = new mx_it[3];
            rollback = new bool[3];
            is_max = new bool[3];
            for (int i = 0; i < 3; i++) {
                rollback[i] = false;
                is_max[i] = false;
            }
        }
        // forbidden:
        InsertGuard(const InsertGuard &) = delete;
        InsertGuard& operator=(const InsertGuard &) = delete;

        // jeśli coś do rollbackowania, to nie jest to dodanie do
        // function set, bo to ostatnia operacja, czyli jeśli sie nie udała
        // to nie zmieniła function_seta
        ~InsertGuard() noexcept {
            for (int i = 0; i < 3; i++) {
                if (rollback[i]) {
                    maximas_set.erase(new_mx_its[i]);
                }
            }
        }

        void drop_rollback() noexcept {
            for (int i = 0; i < 3; i++) {
                rollback[i] = false;
            }
        }

        // kto usunie a i v jesli insert sie nie uda?? albo point type?
        void Insert(fun_set* fun_ptr, mx_set* mx_ptr, point_type &point) {
            fun_set_ptr = fun_ptr;
            mx_set_ptr = mx_ptr;
            fun_set function_set = *fun_ptr;
            mx_set maximas_set = *mx_ptr;

            find_all(point); // może tutaj się przerwać, nic do rollbackowania

            for (int i = 0; i < 3; i++) {
                if (fun_its[i] != function_set.end())
                    is_max[i] = is_maximum(fun_its[i]);
            }
            // ^^mogło coś się przerwać - wciąż nic do rollbackowania

            for (int i = 0; i < 3; i++) {
                if (is_max[i] && mx_its[i] == maximas_set.end()) {
                    // insertuj kopie point type
                    auto ret = maximas_set.insert(point_type(*(fun_its[i]))); 
                    new_mx_its[i] = ret->first; // pobierz iterator wstawionego elementu
                    rollback[i] = true;
                }
            }
            if (fun_its[1] == function_set.end()) {
                function_set.insert(point);
            }
            else {
                (*(fun_its[1])).change_val(point.val().copy());
                delete point; // no throw (point juz sie nie przyda)
            }

            // usun maxima ktore juz nimi nie sa
            for (int i = 0; i < 3; i++) {
                if (!is_max[i] && mx_its[i] != maximas_set.end()) {
                    maximas_set.erase(mx_its[i]); // no throw
                }
            }
            drop_rollback();
        }
    };

    
public:
    
    /** iteratory **/

    iterator begin() const noexcept {
        return iterator(function_set.begin(), &function_set);
    }

    iterator end() const noexcept {
        return iterator(function_set.end(), &function_set);
    }

    iterator find(A const &a) const {
        return iterator(function_set.find(a), &function_set);
    }

    mx_iterator mx_begin() const noexcept {
        return iterator(maximas_set.begin(), &maximas_set);
    }
    
    mx_iterator mx_end() const noexcept {
        return iterator(maximas_set.end(), &maximas_set);
    }
    /** 3 KONSTRUKTORKI >w< **/

    FunctionMaxima() {
    }

    // ?
    FunctionMaxima(FunctionMaxima const &fun) {
        *this = fun;
    }

    // ?
    FunctionMaxima operator=(FunctionMaxima  &fun) {
        *this = fun;
    }

    size_type size() const {
        return function_set.size();
    }
    
    /*
    void to_string() {
        size_t s = function_set.size();
        for (auto it = function_set.begin(); it != function_set.end(); it++) {
            std::cout<<(*it).value()<<" ";
        }
    }
    */

    V const& value_at(A const& a) const {
        auto it = function_set.find(a);
        if (it != function_set.end()) {
            return it.arg();
        } else {
            throw InvalidArg();
        }
    }
    
    void set_value(A const& a, V const& v) {
        InsertGuard insert_guard;
        // tutaj bez sensu chyba tworzymy association zeby potem ewentualnie 
        // usunąć, ale w sumie association to tylko taka mała opakówka,
        // i tak argument bedzie usuwany przy wstawianiu czegos na już istniejący
        // taki sam argument
        insert_guard.insert(association(a, v));
    }

    // ???
    void erase(A const& a) {
        function_set.find(/*??*/); // czego szukać??
        function_set.erase(a);
    }
};

template<typename A, typename V>
class FunctionMaxima<A, V>::impl {

    private:
    // data

    public:
    // private from FunMax

};