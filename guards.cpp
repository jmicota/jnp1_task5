class InsertMxGuard {
        private:
        using mx_set = FunctionMaxima<A, V>::mx_set; 
        using point_type = FunctionMaxima<A, V>::point_type;

        mx_set* mx_set_ptr;
        std::vector<point_type> to_insert;
        std::vector<point_type> to_erase;
        std::vector<bool> rollback;


        public:
        InsertMxGuard() : mx_set_ptr(nullptr), rollback(nullptr), to_insert(nullptr) {}
        // forbidden:
        InsertMxGuard(const InsertMxGuard &) = delete;
        InsertMxGuard& operator=(const InsertMxGuard &) = delete;

        ~InsertMxGuard() noexcept {
            for (int i = 0; i < to_insert.size(); i++) {
                if (rollback[i]) {
                    // fix stuff
                }
            }
        }

        void drop_rollback() noexcept {
            rollback = false;
        }

        void Insert(mx_set* mx_ptr, std::vector<point_type> maximas) {
            mx_set_ptr = mx_ptr;
            mx_set maximas_set = *mx_ptr;
            to_insert = maximas;
            for (int i = 0; i < maximas.size(); i++) { rollback.push_back(false); }

            // jesli wszystko sie uda to musimy usunac:
            // powtorzone identyczne maxima
            // potencjalne poprzednie maximum ktore ma inna val ale arg ten sam co inserted element
            for (int i = 0; i < to_insert.size(); i++) {
                auto it = maximas_set.find(to_insert[i]);

            }
        }
    };

    // przy insercie tworzony
    // probuje insertowac i łapie wyjatki
    // jesli nie bylo wyjatkow - element inserted - rollback=true (jest co rollbackowac)
    // jesli wszystkie inserty przejdą to dropuje rollback i gdy sie destructuje nie cofa zmian
    class InsertGuard {

        private:
        using fun_set = FunctionMaxima<A, V>::fun_set;
        using mx_set = FunctionMaxima<A, V>::mx_set; 
        using point_type = FunctionMaxima<A, V>::point_type;

        fun_set* fun_set_ptr;
        mx_set* mx_set_ptr;
        bool rollback;

        public:
        InsertGuard() : fun_set_ptr(nullptr), mx_set_ptr(nullptr), rollback(false) {}
        // forbidden:
        InsertGuard(const InsertGuard &) = delete;
        InsertGuard& operator=(const InsertGuard &) = delete;

        ~InsertGuard() noexcept {
            if (rollback) {
                // fix stuff
            }
        }

        void drop_rollback() noexcept {
            rollback = false;
        }

        void Insert(fun_set* fun_ptr, mx_set* mx_ptr, point_type &point) {
            fun_set_ptr = fun_ptr;
            mx_set_ptr = mx_ptr;
            fun_set function_set = *fun_set_ptr;
            mx_set maximas_set = *mx_set_ptr;

            auto it = function_set.insert(point); // to może sie wyjebać (multiset zwr iterator)
            // nie wyjebało sie
            rollback = true;
            // sprawdz maxima
            std::vector<point_type*> to_insert;
            // pushnij maxima
            InsertMxGuard insert_mx_guard;
            
            

        }
        

        /*
        bool is_maximum(iter &inserted, iter &to_check) {
            V lval, rval, val;
            A inserted_arg = (*inserted).arg();
            val = (*to_check).value();
            if (to_check == function_set.begin()) {lval = val;}
            else {
                to_check--;

            }
            if (to_check == function_set.end()) {rval = val;}
        }

        void fill_iter_arr(iter arr[], iter &it) {
            arr[2] = iter;
            // iter it2 = it.copy(); //?
            if (it == function_set.begin()) {
                arr[0] = arr[1] = it;
            } else if (it == function_set.end()) {
                arr[3] = arr[4] = it;
            } else {
                A arg = (*it).arg();
                it--;
                if (arg == (*it).arg()) { // same arg
                    if (it == function_set.begin()) {
                        arr[0] = arr[1] = arr[2];
                    }
                    it--;
                } 

            }
        }*/
    };