#include <set>
#include <iterator>
#include <iostream>
#include <exception>
#include <memory>
#include <vector>
#include <iterator>

/**
 * @brief Wyjątek zaznaczony w poleceniu
 *  Informuje o tym, że wywołanie @ref FunctionMaxima<A,V>::value_at()
 *  odbyło się dla argmentu poza dziedziną funkcji
 * 
 * Dziedziczy publicznie po klasie std::exception i nadpisuje what()
 */
class InvalidArg : public std::exception {
    public:
    const char *what() const throw() {
        return "invalid argument value";
    }
};

/**
 * @brief Klasa implementująca interfejs funkcji z polecenia
 * 
 *  Klasa jest przezroczysta na wszelkie wyjątki.
 *  Dostarcza odporne na błędy iteratory. W tym iterator po lokalnych maksimach.
 * 
 *  Wszystkie iteracje wykonywane są w amortyzowanym czasie stałym.
 *      O(n) dla n-elemntów
 * 
 *  Implementacja dostarcza silną odporność na wyjątki.
 * 
 * @tparam A    - Typ argumnetów funkcji
 * @tparam V    - Typ wartości funkcji
 */
template<typename A, typename V>
class FunctionMaxima {
private:
    // deklaracje wstępne
    template<typename Object, typename Container>
    class prot_iterator;

    class InsertGuard;

    class association;

    // niestandardowe operatory porónywania dla kontenrów STL

    // porównywanie dla elementów funkcji
    struct fun_comp {
        bool operator()(association const &a, association const &b) const {
            return a.arg() < b.arg();
        }
    };

    // porównywania dla elementów zbioru maksimów
    struct mx_comp {
        bool operator()(association const &a, association const &b) const {
            if (!(a.value() < b.value()) && !(b.value() < a.value()))
                return a.arg() < b.arg();
            return b.value() < a.value();
        }
    };
    // lokalne definicje zbiorów powiązań funkcyjnych
    using fun_set = std::set<association, fun_comp>;
    using mx_set = std::set<association, mx_comp>;

    // jedyne atrybuty funkcji
    fun_set function_set;
    mx_set maximas_set;

    /**
     * @brief Klasa implementująca interfejs "point_type" z zadania.
     * 
     *  Udostępnia podstawowe metody takie jak arg() i value().
     *  Dostarcza niezbędne informacje dla iteratorów o ich "poprawności"
     * 
     *  Zapewnia silną odporność na wyjątki.
     * 
     *  Implementacja przeznaczona dla @tparam A i @tparam V o dużych rozmiarach
     *  gdyż każda kopia wartości przechowywana jest tylko raz dla dowolnej
     *  liczby kopii obiektu tej klasy (smart shared pointer)
     * 
     */
    class association {
        friend class prot_iterator<association, fun_set>;
        friend class prot_iterator<association, mx_set>;
        friend class InsertGuard;
        friend class FunctionMaxima<A,V>;
        
        private:
        // Decyzja implementacyjna ułatwiająca zapewnienie silnej odporności na wyjątki
        // zbiór powiązań funckji rozróźnia powiązania tylko na podst. argumentu.
        // Dla zbior maksimów używana jest metoda insert/erase
        // Zatem wskaźnik na wartość może być zmienny.
        std::shared_ptr<const A> _argument;
        mutable std::shared_ptr<V> _value;

        // ten bool pod wskaźnikiem informuje iteratory o ich poprawności
        // każdy iterator który nie jest już poprawny rzuci wyjątek przy
        // następnym użyciu
        private:
            // tylko do przeszukiwania po argumencie
            // tworzymy atrapę która posiada tymczasowy adres szukanego argumnetu

            // pusta funkcja, aby nie usuwać obiektu
            struct no_delete {
                void operator()(const void *) const noexcept{}
            };
            // aby unikać ekscesywnych kopii argumentu
            // tworzymy atrapę, która zarządza wskaźnikiem o wartości
            // adresu przekazanej referencji, ale nie usuwa obiektu
            explicit
            association(const A *ptr) {
                _argument.reset(ptr, no_delete());
                _value.reset();
                validation = std::make_shared<bool>(true);
            }
            // interfejs podmiany wartości
            void change_val(std::shared_ptr<V> val_ptr) const noexcept {
                _value = val_ptr;
            }
        protected:
            // współdzielony wskaźnik na wartość przechowjącą "poprawność" odwołania
            std::shared_ptr<bool> validation;

            association() = delete; // nie można tworzyć pustych powiązań

            // konstruktor nowego powiązania kopiujący przekazane wartości
            association(A const & _arg, V const & _val){
                _argument = std::make_shared<A>(_arg);
                _value = std::make_shared<V>(_val);
                validation = std::make_shared<bool>(true);
            }

        public:
            // konstruktor kopiujący
            association(const association &ass) {
                this->_argument = ass._argument;
                this->_value = ass._value;
                this->validation = ass.validation;
            }
            // podstawowy interfejs
            A const& arg() const noexcept { return *_argument; }
            V const& value() const noexcept { return *_value; }
            
            // Destruktort informuje wszystkie prot_iteratory, że są one
            // niepoprawne ustawiając wartość pod @p validation na false
            ~association() noexcept {
                *validation = true;
            }
    };
    // Dalsze definicje rządanych typów
    public:
    using point_type = association;
    using size_type = typename std::set<point_type>::size_type;

    private:
    /**
     * Klasa zapewniająca silną odporność na wyjątki dla dodawania nowego
     * elementu w obu zbiorach.
     * 
     * Implementacja w ramach inwencji RAII.
     * 
     */
    class InsertGuard {
        private:
        using fun_set = FunctionMaxima<A, V>::fun_set;
        using mx_set = FunctionMaxima<A, V>::mx_set; 
        using point_type = FunctionMaxima<A, V>::point_type;
        using fun_it = typename fun_set::iterator;
        using mx_it = typename mx_set::iterator;
        
        fun_set *fun_set_ptr;
        mx_set *mx_set_ptr;
        fun_it fun_its[3];
        mx_it mx_its[3];
        mx_it new_mx_its[3];
        bool rollback[3];
        bool is_max[3];

        // dla elementu jeszcze nie istniejacego / zmienionego podajemy
        //  jego potencjalna przyszla wartosc oraz jej pozycje od sprawdzanego elementu
        // 0->na lewo, 1->(sprawdzany element), 2->na prawo
        bool is_maximum(typename fun_set::iterator itt, int pos, std::shared_ptr<V> val) {
            V *left, *right, *self;

            if (pos == 1 && itt == fun_set_ptr->end()) { // wstawiany el. nieistniejacy w secie
                self = val.get();
                left = right = self;
                // el. nieistniejacy w secie => pobieramy pozycje ze zmiennych globalnych
                if (fun_its[0] != fun_set_ptr->end())
                    left = (*fun_its[0])._value.get();
                if (fun_its[2] != fun_set_ptr->end())
                    right = (*fun_its[2])._value.get();
            }
            else {
                if (pos == 1)
                    self = val.get();
                else 
                    self = (*itt)._value.get();

                if (pos == 0)
                    left = val.get();
                else if (itt == fun_set_ptr->begin())
                    left = self;
                else {
                    itt--;
                    left = (*itt)._value.get();
                    itt++;
                }

                itt++;
                if (pos == 2)
                    right = val.get();
                else if (itt == fun_set_ptr->end())
                    right = self;
                else
                    right = (*itt)._value.get();
                itt--;
            }

            return !(*self < *right) && !(*self < *left);  
        }

        // intencjonalne przekazanie kopii iteratora w argumencie
        fun_it new_prev(fun_it it) {
            if (it != fun_set_ptr->begin())
                return --it;
            return fun_set_ptr->end();
        }
        
        fun_it new_next(fun_it it) {
            if (it != fun_set_ptr->end())
                return ++it;
            else return it;
        }

        void find_all(point_type &new_point) {
            // for (int i = 0; i < 3; i++)
            //     fun_its[i] = fun_set_ptr->end();
            
            fun_its[1] = fun_set_ptr->find(new_point); // znajduje argument taki jak new_point

            if (fun_its[1] == fun_set_ptr->end()) {
                // nie ma takiego argumentu jeszcze
                fun_its[2] = fun_set_ptr->upper_bound(new_point);
                fun_its[0] = new_prev(fun_its[2]);
            } else {
                // jest juz taki argument
                fun_its[0] = new_prev(fun_its[1]);
                fun_its[2] = new_next(fun_its[1]);
            }

            for (int i = 0; i < 3; i++) {
                if (fun_its[i] != fun_set_ptr->end())
                    mx_its[i] = mx_set_ptr->find(*(fun_its[i]));
                else
                    mx_its[i] = mx_set_ptr->end();

                new_mx_its[i] = mx_set_ptr->end();
            }
        }

        public:
        InsertGuard() {
            for (int i = 0; i < 3; i++) {
                rollback[i] = false;
                is_max[i] = false;
            }
        }
        // niedozwolone konstruktory
        InsertGuard(const InsertGuard &) = delete;
        InsertGuard& operator=(const InsertGuard &) = delete;

        // jeśli coś do rollbackowania, to nie jest to dodanie do
        // function set, bo to ostatnia operacja, czyli jeśli sie nie udała
        // to nie zmieniła function_seta
        ~InsertGuard() noexcept {
            for (int i = 0; i < 3; i++) {
                if (rollback[i]) {
                    mx_set_ptr->erase(new_mx_its[i]);
                }
            }
        }

        // Zerujemy flagi przywracania jednocześnie pieczętując operację dodawania
        void drop_rollback() noexcept {
            for (int i = 0; i < 3; i++) {
                rollback[i] = false;
            }
        }

        // Główna operacja obiektu
        void insert(fun_set* fun_ptr, mx_set* mx_ptr, point_type &&point) {
            fun_set_ptr = fun_ptr;
            mx_set_ptr = mx_ptr;

            find_all(point); // może tutaj się przerwać, nic do rollbackowania

            if (fun_set_ptr->empty()) {
                is_max[1] = true;
            } 
            else {
                if (fun_its[0] != fun_set_ptr->end())
                    is_max[0] = is_maximum(fun_its[0], 2, point._value);
                
                // jesli nie ma tego argumentu to is_maximum dziala inaczej
                is_max[1] = is_maximum(fun_its[0], 1, point._value);
                
                if (fun_its[2] != fun_set_ptr->end())
                    is_max[2] = is_maximum(fun_its[2], 0, point._value);
            }
            // ^^mogło coś się przerwać - wciąż nic do rollbackowania

            if (is_max[1]) { // nowy element jest maximum
                auto ret = mx_set_ptr->insert(point); 
                new_mx_its[1] = ret.first; // pobierz iterator wstawionego elementu
                rollback[1] = true;
            }
            // dla elementu z lewej i z prawej
            for (int i = 0; i < 3; i++) {
                if (i == 0 || i == 2) {
                    if (is_max[i] && mx_its[i] == mx_set_ptr->end()) {
                        auto ret = mx_set_ptr->insert(*(fun_its[i])); 
                        new_mx_its[i] = ret.first;
                        rollback[i] = true;
                    }
                }
            }

            // dodanie elementu do funkcji (ostatnia funkcja mogaca rzucic wyjatek)
            if (fun_its[1] == fun_set_ptr->end()) {
                fun_set_ptr->insert(point);
            }
            else {
                (*(fun_its[1])).change_val(point._value);
            }

            // usun maxima ktore juz nimi nie sa
            for (int i = 0; i < 3; i++) {
                if (i == 0 || i == 2) {
                    if (!is_max[i] && mx_its[i] != mx_set_ptr->end())
                        mx_set_ptr->erase(mx_its[i]); // no throw
                }
            }
            // usun ewentualne poprzednie nadpisane maximum
            if (mx_its[1] != mx_set_ptr->end()) {
                mx_set_ptr->erase(mx_its[1]); // no throw
            }
            drop_rollback();
        }

        void erase(fun_set* fun_ptr, mx_set* mx_ptr, point_type &&point) {
            fun_set_ptr = fun_ptr;
            mx_set_ptr = mx_ptr;
            for (int i = 0; i < 3; i++) {
                new_mx_its[i] = mx_set_ptr->end();
            }
            fun_it itt = fun_set_ptr->find(point);
            
            if (itt != fun_set_ptr->end()) {
                fun_it left_itt = new_prev(itt);
                fun_it right_itt = new_next(itt);
                std::shared_ptr<V> left, right;
                if (itt != fun_set_ptr->begin()) {
                    left = (*left_itt)._value;
                }
                else left.reset();
                
                if (right_itt != fun_set_ptr->end()) {
                    right = (*right_itt)._value;
                } 
                else right.reset();
                
                if (left_itt != fun_set_ptr->end()) {
                    // sprawdzamy lewego
                    is_max[0] = is_maximum(left_itt, 2, right);
                }

                if (right_itt != fun_set_ptr->end()) {
                    // sprawdzamy prawego
                    is_max[2] = is_maximum(right_itt, 0, left);
                }

                mx_its[0] = mx_set_ptr->find(*left_itt);
                mx_its[1] = mx_set_ptr->find(*itt);
                mx_its[2] = mx_set_ptr->find(*right_itt);

                if (is_max[0] && left_itt != fun_set_ptr->end() && mx_set_ptr->end() == mx_its[0]) {
                    auto ret = mx_set_ptr->insert(*left_itt);
                    rollback[0] = true;
                    new_mx_its[0] = ret.first;
                }
                if (is_max[2] && right_itt != fun_set_ptr->end() &&  mx_set_ptr->end() == mx_its[2]) {
                    auto ret = mx_set_ptr->insert(*right_itt);
                    rollback[2] = true;
                    new_mx_its[2] = ret.first;
                }

                if (!is_max[0] && left_itt != fun_set_ptr->end() && mx_set_ptr->end() != mx_its[0])
                    mx_set_ptr->erase(mx_its[0]);
                if (mx_its[1] != mx_set_ptr->end())
                    mx_set_ptr->erase(mx_its[1]);
                if (!is_max[2] && right_itt != fun_set_ptr->end() &&  mx_set_ptr->end() != mx_its[2])
                    mx_set_ptr->erase(mx_its[2]);
                
                drop_rollback();
            }
            
            fun_set_ptr->erase(itt);
        }
        
    };

    /**
     * Obudowanie iteratora std::set w dodatkową ochronę przed niepożądanymi
     * operacjami jak end()++, begin()--, *end(), oraz ochrona przed używaniem
     * iteratora, który został unieważniony (w skutek set.clear() lub set.erease())
     * 
     * @throw InvalidatedIterator gdy iterator został unieważniony
     * @throw InvalidIteratorDereference gdy wykonujemy dereferencję iteratora 
     *        wskazującego za koniec setu
     * @throw InvalidIteratorMove gdy wykonujemy inkrementację (dekrementację)
     *        iteratora za końcem (na początku) setu
     * 
     * Spełnia wymogi std::bidirectional_iterator_tag
     * 
     * Dodatkowe info:
     *  zakładamy że T posiada "std::shared_ptr<bool> validation"
     */
    template<typename Object, typename Container>
    class prot_iterator : public std::iterator<std::bidirectional_iterator_tag, Object> {
        // Iterator ma swój współdzielony wskaźnik, by ten nie zginął
        // póki jakikolwiek iterator może wskazywać na usunięty obiekt
        std::shared_ptr<bool> validation;

        // iterator, którego używamy przy poruszać się po kontenerze
        typename Container::iterator itt;

        bool past_the_end;
        // wskaźnik na kontener, by móc efektywnie sprawdzać jego początek i koniec
        const Container * iterating;

        // DEFINICJE WYJĄTKÓW ITERATORA

        public:
        // Iterator został unieważniony
        class InvalidatedIterator : public std::exception {
            public:
            const char *what() const throw() override {
                return "This iterator has been ivalidated due to cointainer management.";
            }
        };

        // Próba dereferencji iteratora wskazujacego na end()
        class InvalidIteratorDereference : public std::exception {
            public:
            const char *what() const throw() override {
                return "Dereference of past-the-end iterator";
            }
        };

        // Próba zrobienia end()++ lub begin()--
        class InvalidIteratorMove : public std::exception {
            public:
            const char *what() const throw() override {
                return "Inc (dec) for past-the-end (begin) iterator [undefined behaviour].";
            }
        };

        private:
        // Rzuca wyjątek jeżeli iterator jest unieważniony
        void check_validation() const {
            if (!past_the_end && *validation == false) 
                throw InvalidatedIterator();
        }

        public:
        // Konstruktor otaczający przekazany iterator i jego kontener
        explicit
        prot_iterator(typename Container::iterator const &itt, const Container * iterating) noexcept{
            this->itt = itt;
            this->iterating = iterating;
            this->past_the_end = (itt == iterating->end());
            if (!past_the_end)
                this->validation = (*itt).validation;
            else
                this->validation = std::make_shared<bool>(false);
        }

        prot_iterator(const prot_iterator &pit) = default;
        // pozostałe konstruktory zostają domyślne
        
        
        // PODSTAWOWE NADPISANE OPERATORY //
        public:
        const Object &
        operator*() const {
            if (past_the_end)
                throw InvalidIteratorDereference();
            
            check_validation();
            return *itt;
        }

        const Object *
        operator->() const {
            if (past_the_end)
                throw InvalidIteratorDereference();
            check_validation();
            return itt.operator->();
        }

        prot_iterator &
        operator++() {
            check_validation();
            // set przy usuwaniu usuwa swoje elementy czyli
            // ten kawałek kodu ruszy <=> set istnieje
            if (past_the_end)
                throw InvalidIteratorMove();

            ++itt;
            if (itt == iterating->end())
                past_the_end = true;
            
            if (!past_the_end)
                validation = (*itt).validation;

            return *this;
        }

        prot_iterator
        operator++(int) {
            prot_iterator tmp = *this;
            ++(*this); // validation checking
            return tmp;
        }

        prot_iterator &
        operator--() {
            check_validation();

            if (itt == iterating->begin())
                throw InvalidIteratorMove();
            
            else if (past_the_end)
                past_the_end = false;

            --itt;

            if (!past_the_end)
                validation = (*itt).validation;

            return *this;
        }   

        prot_iterator
        operator--(int) {
            prot_iterator tmp = *this;
            --(*this);
            return tmp;
        }

        bool
        operator==(const prot_iterator &x) const {
            check_validation();
            x.check_validation();
            return itt == x.itt;
        }

        bool
        operator!=(const prot_iterator &x) const {
            check_validation();
            x.check_validation();
            return itt != x.itt;
        }
    };

    public:
    // Definicje typów iteratorów
    using mx_iterator = prot_iterator<point_type, mx_set>;
    using iterator = prot_iterator<point_type, fun_set>;

    // Podstawowe funkcje przekazjące iteratory

    iterator begin() const noexcept {
        return iterator(function_set.begin(), &function_set);
    }

    iterator end() const noexcept {
        return iterator(function_set.end(), &function_set);
    }

    iterator find(A const &a) const {
        point_type search(&a);
        return iterator(function_set.find(search), &function_set);
    }

    mx_iterator mx_begin() const noexcept {
        return mx_iterator(maximas_set.begin(), &maximas_set);
    }
    
    mx_iterator mx_end() const noexcept {
        return mx_iterator(maximas_set.end(), &maximas_set);
    }

    // Funkcja to tylko 2 sety więc konstruktor jest pusty
    FunctionMaxima() {}

    // konstrktor i operator kopiujący są domyślne <=> kopia setów
    FunctionMaxima(FunctionMaxima const &fun) = default;

    FunctionMaxima &operator=(FunctionMaxima const &fun) = default;

    // Liczba powiązań reprezentowanych w funckji
    size_type size() const {
        return function_set.size();
    }
    
    // Przekazanie wartości pod @p a rzuca wyjątek InvalidArg()
    V const& value_at(A const& a) const {
        point_type searching(&a); // RAII
        auto it = function_set.find(searching); // możliwy wyjątek
        if (it != function_set.end()) {
            return it->value();
        } else {
            throw InvalidArg();
        }
    }
    
    // Dodaje lub podmienia wartosc pod argumentem V
    void set_value(A const& a, V const& v) {
        InsertGuard insert_guard;
        //insert_guard.insert(&function_set, &maximas_set, std::move(association(a, v)));
        point_type point(a, v);
        insert_guard.insert(&function_set, &maximas_set, std::move(point));
    }

    // Usuwa element
    void erase(A const& a) {
        point_type searching(&a);
        InsertGuard insert_guard;
        insert_guard.erase(&function_set, &maximas_set, std::move(searching));
    }
};